package com.ibrahimrecepserpici.weatherlogger.adapter;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ibrahimrecepserpici.weatherlogger.view.DetailsFragment;
import com.ibrahimrecepserpici.weatherlogger.view.MapFragment;

public class ViewPagerAdapter  extends FragmentStatePagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm,FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i)
        {
            case 0: return new MapFragment();
            case 1: return new DetailsFragment();
            default: return null;

        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position)
        {
            case 0: return "WEATHER MAP";
            case 1: return "DETAILS";
            default: return null;

        }
    }

}
