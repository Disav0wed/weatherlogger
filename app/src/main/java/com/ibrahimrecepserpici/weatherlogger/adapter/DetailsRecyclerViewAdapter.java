package com.ibrahimrecepserpici.weatherlogger.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.ibrahimrecepserpici.weatherlogger.R;
import com.ibrahimrecepserpici.weatherlogger.pojo.CitiesInRange;
import com.ibrahimrecepserpici.weatherlogger.utility.Utils;
import com.ibrahimrecepserpici.weatherlogger.viewholder.CityViewHolder;


public class DetailsRecyclerViewAdapter extends RecyclerView.Adapter {

    private static final String TAG = Utils.class.getSimpleName();
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private CitiesInRange mCitiesInRange;

    public DetailsRecyclerViewAdapter(Context context, CitiesInRange citiesInRange)
    {
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mCitiesInRange = citiesInRange;
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.recyclerview_city_details_row,parent,false);
        return new CityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((CityViewHolder)holder).setIndex(position);
        ((CityViewHolder)holder).tvCityName.setText(mCitiesInRange.getCityList().get(position).getName());
        ((CityViewHolder)holder).tvTemp.setText(String.valueOf((int)Utils.kelvinToCelcius(mCitiesInRange.getCityList().get(position).getMain().getTemp()))+" °C");
        ((CityViewHolder)holder).tvHumidity.setText(String.valueOf(mCitiesInRange.getCityList().get(position).getMain().getHumidity())+" units");
        ((CityViewHolder)holder).tvPressure.setText(String.valueOf(mCitiesInRange.getCityList().get(position).getMain().getPressure())+" units");
        ((CityViewHolder)holder).tvWindSpeed.setText(String.valueOf(mCitiesInRange.getCityList().get(position).getWind().getSpeed())+" kph");
        ((CityViewHolder)holder).tvWindDirection.setText(String.valueOf(mCitiesInRange.getCityList().get(position).getWind().getDeg())+"°");

        String weatherStatus = mCitiesInRange.getCityList().get(position).getWeather().get(0).getMain();
        switch (weatherStatus)
        {
            case "Clear":
                ((CityViewHolder)holder).ivweatherIcon.setImageResource(R.drawable.weather_icon_sunny);
                break;
            case "Rain":
                ((CityViewHolder)holder).ivweatherIcon.setImageResource(R.drawable.weather_icon_rainy);
                break;
            case "Clouds":
                ((CityViewHolder)holder).ivweatherIcon.setImageResource(R.drawable.weather_icon_cloudy);
                break;
            case "Snow":
                ((CityViewHolder)holder).ivweatherIcon.setImageResource(R.drawable.weather_icon_snowy);
                break;
            default:
                Log.e(TAG,"Case Mismatch. Received Case Name : "+weatherStatus.toString());
                break;
        }
    }
    @Override
    public int getItemCount() {
        return mCitiesInRange.getCityList().size();
    }
}
