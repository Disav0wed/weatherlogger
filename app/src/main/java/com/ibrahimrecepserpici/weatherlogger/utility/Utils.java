package com.ibrahimrecepserpici.weatherlogger.utility;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.ibrahimrecepserpici.weatherlogger.R;
import com.ibrahimrecepserpici.weatherlogger.database.CityDataModel;
import com.ibrahimrecepserpici.weatherlogger.pojo.CitiesInRange;
import com.ibrahimrecepserpici.weatherlogger.pojo.City;
import com.ibrahimrecepserpici.weatherlogger.pojo.Coord;
import com.ibrahimrecepserpici.weatherlogger.pojo.Main;
import com.ibrahimrecepserpici.weatherlogger.pojo.Weather;
import com.ibrahimrecepserpici.weatherlogger.pojo.Wind;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    public static BitmapDescriptor createWeatherIcon(String temp,String cityName, String weatherStatus, int textSize, Activity activity) {


        Paint textPaint = new Paint();
        textPaint.setTextSize(textSize);
        textPaint.setFakeBoldText(true);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextAlign(Paint.Align.CENTER);

        float textWidth = textPaint.measureText(cityName);
        float textHeight = textPaint.getTextSize();
        int iconWidth = 200;
        int iconHeight = 200;
        //How Many Lines of Text Will be In The Marker
        int lineCount =2;
        int width = (int) (textWidth);
        int height = ((int) (textHeight))*lineCount +iconHeight;
        if(iconWidth>textWidth)
        {
            width = iconWidth;
        }

        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);

        Bitmap icon;


        switch (weatherStatus)
        {
            case "Clear":

                icon= BitmapFactory.decodeResource(activity.getResources(),
                        R.drawable.weather_icon_sunny);
                icon= Bitmap.createScaledBitmap(icon,iconWidth,iconHeight,false);

                break;
            case "Rain":
                icon= BitmapFactory.decodeResource(activity.getResources(),
                        R.drawable.weather_icon_rainy);
                icon= Bitmap.createScaledBitmap(icon,iconWidth,iconHeight,false);
                break;
            case "Clouds":
                icon= BitmapFactory.decodeResource(activity.getResources(),
                        R.drawable.weather_icon_cloudy);
                icon= Bitmap.createScaledBitmap(icon,iconWidth,iconHeight,false);
                break;
            case "Snow":
                icon= BitmapFactory.decodeResource(activity.getResources(),
                        R.drawable.weather_icon_snowy);
                icon= Bitmap.createScaledBitmap(icon,iconWidth,iconHeight,false);
                break;
                default:
                    Log.e(TAG,"Case Mismatch. Received Case Name : "+weatherStatus.toString());
                    icon= BitmapFactory.decodeResource(activity.getResources(),
                            R.drawable.weather_icon_sunny);
                    icon= Bitmap.createScaledBitmap(icon,iconWidth,iconHeight,false);

                    break;
        }



        textPaint.setColor(activity.getResources().getColor(R.color.colorLightPink));
        canvas.drawRoundRect(new RectF(0,lineCount*(int)textHeight+10,width,height), 15,15,textPaint);


        canvas.drawBitmap(icon, new Rect(0,-canvas.getClipBounds().top,canvas.getClipBounds().right,canvas.getClipBounds().bottom),canvas.getClipBounds(),null);
        canvas.translate(0, height);
        // Set a background in order to see the
        // full size and positioning of the bitmap.
        // Remove that for a fully transparent icon.
        textPaint.setColor(Color.BLACK);
        canvas.drawText(temp, width/2, -textHeight, textPaint);
        canvas.drawText(cityName,width/2,-textHeight*2,textPaint);

        BitmapDescriptor result = BitmapDescriptorFactory.fromBitmap(image);
        return result;
    }

    public static double kelvinToCelcius(double kelvin)
    {
        return (double) (kelvin -273.15);
    }

    public static List<CityDataModel> cityInfoModelConverter(CitiesInRange citiesInRange)
    {
        List<CityDataModel> resultList = new ArrayList<CityDataModel>();

        for (City i : citiesInRange.getCityList())
        {
            CityDataModel tempCityDataModel = new CityDataModel(
                    String.valueOf(i.getCoord().getLat()),
                    String.valueOf(i.getCoord().getLon()),
                    String.valueOf(i.getMain().getTemp()),
                    i.getName(),
                    String.valueOf(i.getWind().getSpeed()),
                    String.valueOf(i.getWind().getDeg()),
                    String.valueOf(i.getMain().getHumidity()),
                    String.valueOf(i.getMain().getPressure()),
                    i.getWeather().get(0).getMain(),timeStamp()
            );
            resultList.add(tempCityDataModel);
        }

        return  resultList;
    }

    public static List<City> cityInfoModelConverter(List<CityDataModel> list)
    {
        List<City> resultList = new ArrayList<City>();

        for(CityDataModel c: list)
        {

            City tempCity = new City();
            tempCity.setWind(new Wind());
            tempCity.setMain(new Main());
            tempCity.setCoord(new Coord());
            tempCity.setWeather(new ArrayList<Weather>());
            Weather tempWeather = new Weather();
            tempWeather.setMain(c.getWeatherStatus());

            tempCity.setName(c.getCityName());
            tempCity.getMain().setTemp(Double.valueOf(c.getTemperature()));
            tempCity.getWind().setSpeed(Double.valueOf(c.getWindSpeed()));
            tempCity.getWind().setDeg(Integer.valueOf(c.getWindDirection()));
            tempCity.getMain().setHumidity(Integer.valueOf(c.getHumidity()));
            tempCity.getMain().setPressure(Integer.valueOf(c.getPressure()));
            tempCity.getCoord().setLat(Double.valueOf(c.getLatitude()));
            tempCity.getCoord().setLon(Double.valueOf(c.getLongitude()));
            tempCity.getWeather().add(tempWeather);
            resultList.add(tempCity);

        }


        return  resultList;
    }



    public static String timeStamp()
    {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return sdf.format(timestamp);


    }


}
