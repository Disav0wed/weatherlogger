package com.ibrahimrecepserpici.weatherlogger.network;


import com.ibrahimrecepserpici.weatherlogger.pojo.CitiesInRange;
import com.ibrahimrecepserpici.weatherlogger.pojo.City;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitInterface {

//API key is ef4b95610e6c6105424d5d1a83482180
    @GET("/data/2.5/weather")
    Observable<City> getWeatherInfo(@Query("q") String city, @Query("appid") String appId);

//Example : http://api.openweathermap.org/data/2.5/find?lat=55.5&lon=37.5&cnt=10&appid=ef4b95610e6c6105424d5d1a83482180
    @GET("/data/2.5/find")
    Observable<CitiesInRange> getWeatherInCircle(@Query("lat") String latitude, @Query("lon") String longitude, @Query("cnt")String count, @Query("appid") String appId);
}
