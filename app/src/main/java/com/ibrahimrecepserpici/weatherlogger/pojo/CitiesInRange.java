package com.ibrahimrecepserpici.weatherlogger.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CitiesInRange
{
    @SerializedName("count")
    private String count;

    @SerializedName("cod")
    private String cod;

    @SerializedName("message")
    private String message;

    @SerializedName("list")
    private List<City> cityList = new ArrayList<City>();

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    public String getCod ()
    {
        return cod;
    }

    public void setCod (String cod)
    {
        this.cod = cod;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public List<City> getCityList()
    {
        return cityList;
    }

    public void setCityList(List<City> cityList)
    {
        this.cityList = cityList;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [count = "+count+", cod = "+cod+", message = "+message+", cityList = "+ cityList +"]";
    }
}