package com.ibrahimrecepserpici.weatherlogger.presenter;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.ibrahimrecepserpici.weatherlogger.database.CityDataModel;
import com.ibrahimrecepserpici.weatherlogger.database.DatabaseManager;
import com.ibrahimrecepserpici.weatherlogger.model.GoogleApiManager;
import com.ibrahimrecepserpici.weatherlogger.model.OpenWeatherApiManager;
import com.ibrahimrecepserpici.weatherlogger.pojo.CitiesInRange;
import com.ibrahimrecepserpici.weatherlogger.pojo.City;
import com.ibrahimrecepserpici.weatherlogger.rxbus.RxCitiesInRangeBus;
import com.ibrahimrecepserpici.weatherlogger.utility.Utils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class MapFragmentPresenter  implements GoogleApiManager.LocationCallBack , OpenWeatherApiManager.OpenWeatherCallback {

    private static final String TAG = MapFragmentPresenter.class.getSimpleName();
    private MapFragmentView mMapFragmentView;
    private GoogleApiManager mGoogleApiManager;
    private OpenWeatherApiManager mOpenWeatherApiManager;
    private DatabaseManager mDatabaseManager;
    private Context mContext;
    private Observer<List<CityDataModel>> databaseResultObserver;
    private List<CityDataModel> mCityDataModels;


    public MapFragmentPresenter(MapFragmentView mapFragmentView, Activity activity, Context context)
    {
        if(mapFragmentView == null) throw new NullPointerException("view can not be NULL");
        if(mapFragmentView == null) throw new NullPointerException("Activity can not be NULL");
        if(context == null) throw new NullPointerException("context can not be NULL");
        this.mContext = context;
        this.mGoogleApiManager = new GoogleApiManager(activity,this);
        this.mOpenWeatherApiManager = new OpenWeatherApiManager(this);
        this.mMapFragmentView = mapFragmentView;
        this.mMapFragmentView.generateMap();
        mDatabaseManager = new DatabaseManager(mContext);
        mCityDataModels = new ArrayList<CityDataModel>();
        databaseResultObserver = new Observer<List<CityDataModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<CityDataModel> cityDataModels) {

                Log.e(TAG,"Count is : "+ cityDataModels.size());
                if(cityDataModels.size()>0)
                {

                    Toast.makeText(mContext, "Information Loaded From Database" , Toast.LENGTH_SHORT).show();
                    CitiesInRange ct = new CitiesInRange();
                    ct.setCityList(Utils.cityInfoModelConverter(cityDataModels));
                    mapFragmentView.updateWeatherData(ct);
                    RxCitiesInRangeBus.getInstance().publishSubject.onNext(ct);
                    mCityDataModels = cityDataModels;

                    mGoogleApiManager.removeLocationUpdates();


                }else
                    {
                        Toast.makeText(mContext, "No Data Available" , Toast.LENGTH_SHORT).show();
                    }


            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

    }


    public void saveResultsToDatabase()
    {
        mDatabaseManager.DeleteAll();

        if (mCityDataModels.size()>0)
        {
            for (CityDataModel c: mCityDataModels)
            {
                mDatabaseManager.Insert(c);
            }

            Toast.makeText(mContext, "Data is saved to Local Database." , Toast.LENGTH_SHORT).show();

        }else
            {
                Toast.makeText(mContext, "No Data To Save Yet." , Toast.LENGTH_SHORT).show();

            }


    }

    @Override
    public void onMapReady()
    {
        connectToLocationService();

    }

    public void requestFromDatabase()
    {
        mDatabaseManager.RequestAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(databaseResultObserver);

    }

    public void connectToLocationService() {
        Log.d(TAG, "connectToLocationService: Called");
        this.mGoogleApiManager.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mMapFragmentView.updateLocationOnMap(location);
        mOpenWeatherApiManager.fetchWeatherAsync(new LatLng(location.getLatitude(),location.getLongitude()));
    }

    @Override
    public void onCityInfoReceived(City city) {

    }

    @Override
    public void onNearestCitiesInfoRecieved(CitiesInRange citiesInRange) {

        mMapFragmentView.updateWeatherData(citiesInRange);
        Toast.makeText(mContext, "Data Received from OpenWeather Api." , Toast.LENGTH_SHORT).show();
        mCityDataModels = Utils.cityInfoModelConverter(citiesInRange);
    }

    public interface MapFragmentView
    {
        void generateMap();
        void updateLocationOnMap(Location location);
        void updateWeatherData(CitiesInRange citiesInRange);
    }

}

