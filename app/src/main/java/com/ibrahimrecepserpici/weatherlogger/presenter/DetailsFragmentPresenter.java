package com.ibrahimrecepserpici.weatherlogger.presenter;

import android.content.Context;
import android.util.Log;

import com.ibrahimrecepserpici.weatherlogger.database.AppDatabase;
import com.ibrahimrecepserpici.weatherlogger.database.CityDataModel;
import com.ibrahimrecepserpici.weatherlogger.model.DetailsFragmentDataHandler;
import com.ibrahimrecepserpici.weatherlogger.pojo.CitiesInRange;
import com.ibrahimrecepserpici.weatherlogger.utility.Utils;

public class DetailsFragmentPresenter implements DetailsFragmentDataHandler.DataHandlerCallback {
    private static final String TAG = Utils.class.getSimpleName();
    private DetailsFragmentView mDetailsFragmentView;
    private DetailsFragmentDataHandler mDetailsFragmentDataHandler;
    private Context mContext;

    public DetailsFragmentPresenter(DetailsFragmentView detailsFragmentView, Context context)
    {
        this.mDetailsFragmentView = detailsFragmentView;
        this.mDetailsFragmentDataHandler = new DetailsFragmentDataHandler(this);
        this.mContext = context;

    }

    @Override
    public void onWeatherInformationReceived(CitiesInRange citiesInRange) {

        mDetailsFragmentView.updateWeatherData(citiesInRange);

    }




    public interface DetailsFragmentView
    {
        void updateWeatherData(CitiesInRange citiesInRange);
    }


}
