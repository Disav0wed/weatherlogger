package com.ibrahimrecepserpici.weatherlogger.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.ibrahimrecepserpici.weatherlogger.R;
import com.ibrahimrecepserpici.weatherlogger.adapter.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    FragmentManager fragmentManager;
    TabLayout pagerTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init
        fragmentManager = getSupportFragmentManager();
        pagerTabLayout = (TabLayout) findViewById(R.id.TLayout);
        viewPager = (ViewPager)findViewById(R.id.VPager);
        viewPagerAdapter = new ViewPagerAdapter(fragmentManager);
        viewPager.setAdapter(viewPagerAdapter);
        pagerTabLayout.setupWithViewPager(viewPager);


    }


}
