package com.ibrahimrecepserpici.weatherlogger.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ibrahimrecepserpici.weatherlogger.R;
import com.ibrahimrecepserpici.weatherlogger.adapter.DetailsRecyclerViewAdapter;
import com.ibrahimrecepserpici.weatherlogger.pojo.CitiesInRange;
import com.ibrahimrecepserpici.weatherlogger.presenter.DetailsFragmentPresenter;

public class DetailsFragment extends Fragment implements DetailsFragmentPresenter.DetailsFragmentView {

    View detailsFragmentView;
    RecyclerView detailsRecyclerView;
    DetailsRecyclerViewAdapter detailsRecyclerViewAdapter;
    DetailsFragmentPresenter detailsFragmentPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        detailsFragmentView = inflater.inflate(R.layout.details_fragment_layout,container,false);

        //Initialize RecyclerView
        detailsRecyclerView = detailsFragmentView.findViewById(R.id.RecyclerViewDetails);
        detailsFragmentPresenter = new DetailsFragmentPresenter(this, getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        detailsRecyclerView.setLayoutManager(layoutManager);


        return  detailsFragmentView;
    }


    @Override
    public void updateWeatherData(CitiesInRange citiesInRange) {

        detailsRecyclerViewAdapter = new DetailsRecyclerViewAdapter(getContext(),citiesInRange);
        detailsRecyclerView.setAdapter(detailsRecyclerViewAdapter);



    }
}
