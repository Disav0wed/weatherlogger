package com.ibrahimrecepserpici.weatherlogger.view;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.button.MaterialButton;
import com.ibrahimrecepserpici.weatherlogger.R;
import com.ibrahimrecepserpici.weatherlogger.model.GoogleApiManager;
import com.ibrahimrecepserpici.weatherlogger.pojo.CitiesInRange;
import com.ibrahimrecepserpici.weatherlogger.pojo.City;
import com.ibrahimrecepserpici.weatherlogger.presenter.MapFragmentPresenter;
import com.ibrahimrecepserpici.weatherlogger.utility.Utils;

public class MapFragment extends Fragment implements OnMapReadyCallback, MapFragmentPresenter.MapFragmentView {

    private static final String TAG = GoogleApiManager.class.getSimpleName();
    private View mapFragmentView;
    private MapView mapView;
    private MaterialButton saveButton, loadButton;
    private GoogleMap mMap;
    private MapFragmentPresenter presenter;
    private Marker currentPositionMarker;
    private Bundle msavedInstanceState;
    private Marker[] cityMarkers;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mapFragmentView = inflater.inflate(R.layout.map_fragment_layout, container, false);
        msavedInstanceState = savedInstanceState;
        presenter = new MapFragmentPresenter(this,getActivity(),getContext());
        saveButton = mapFragmentView.findViewById(R.id.buttonSave);
        loadButton = mapFragmentView.findViewById(R.id.buttonLoad);

        //Init Click Listeners
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.saveResultsToDatabase();
            }
        });

        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG,"loadButton onClick: called");
                presenter.requestFromDatabase();
            }
        });


        return  mapFragmentView;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.e(TAG,"onMapReady Called");
        mMap = googleMap;
        // Add a marker in Istanbul and move the camera
       // LatLng istanbul = new LatLng(41, 28.97);
        //currentPositionMarker = mMap.addMarker(new MarkerOptions().position(istanbul).title("Marker in Istanbul"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(istanbul));
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(istanbul, 10.0f));
        //Disable Scrolling
        //mMap.getUiSettings().setScrollGesturesEnabled(false);
        presenter.onMapReady();

        /*MarkerOptions test = new MarkerOptions();
        test.icon(Utils.createWeatherIcon("17 ℃","Istanbul","snowy",45, getActivity())).position(istanbul)
        .title("TESTTITLE");
        mMap.addMarker(test);*/

    }

    @Override
    public void generateMap() {
        Log.e(TAG,"generateMap Called");
        mapView = (MapView) mapFragmentView.findViewById(R.id.mapView);
        mapView.onCreate(msavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

    }

    @Override
    public void updateLocationOnMap(Location location) {

        if(currentPositionMarker !=null)
        {
            currentPositionMarker.remove();
        }

        LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
        currentPositionMarker = mMap.addMarker(new MarkerOptions().position(myLocation)
                .title("My Location"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 10.0f));
    }

    @Override
    public void updateWeatherData(CitiesInRange citiesInRange)
    {
        if(cityMarkers == null)
        {
            cityMarkers = new Marker[citiesInRange.getCityList().size()];
        }

        for(int x = 0; x<citiesInRange.getCityList().size();x++)
        {
            if(cityMarkers[x] != null)
            {
                cityMarkers[x].remove();
                cityMarkers[x]=null;
            }
            LatLng cityCoords = new LatLng(citiesInRange.getCityList().get(x).getCoord().getLat()
                    , citiesInRange.getCityList().get(x).getCoord().getLon());
            String cityName = citiesInRange.getCityList().get(x).getName();
            double temp = Utils.kelvinToCelcius(citiesInRange.getCityList().get(x).getMain().getTemp());
            String weatherStatus = citiesInRange.getCityList().get(x).getWeather().get(0).getMain();

            cityMarkers[x]= mMap.addMarker(new MarkerOptions()
                    .position(cityCoords)
                    .title(cityName)
                    .icon(Utils.createWeatherIcon(String.valueOf((int)temp),cityName,weatherStatus,45,getActivity()))
            );


        }

    }
}

