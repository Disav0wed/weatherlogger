package com.ibrahimrecepserpici.weatherlogger.viewholder;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.ibrahimrecepserpici.weatherlogger.R;
import com.ibrahimrecepserpici.weatherlogger.database.CityDataModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private static final String TAG = CityViewHolder.class.getSimpleName();
    private Context mContext;
    public TextView tvCityName, tvTemp, tvWindSpeed, tvWindDirection, tvHumidity, tvPressure;
    public AppCompatImageView ivweatherIcon;
    private MaterialButton button_Expand;
    private LinearLayout linearLayout_childItems;
    private int index;

    public CityViewHolder(@NonNull View itemView) {
        super(itemView);

        mContext = itemView.getContext();
        tvCityName = itemView.findViewById(R.id.tv_recyclerview_row_city_name);
        tvTemp = itemView.findViewById(R.id.tv_recyclerview_row_temp);
        tvWindSpeed = itemView.findViewById(R.id.tv_recyclerview_row_windSpeed);
        tvWindDirection = itemView.findViewById(R.id.tv_recyclerview_row_windDirection);
        tvPressure = itemView.findViewById(R.id.tv_recyclerview_row_pressure);
        tvHumidity = itemView.findViewById(R.id.tv_recyclerview_row_humidity);
        ivweatherIcon = itemView.findViewById(R.id.iv_recyclerview_row_weather_icon);


        button_Expand = itemView.findViewById(R.id.bttn_recyclerview_row_expand);
        linearLayout_childItems = itemView.findViewById(R.id.ll_child);
        linearLayout_childItems.setVisibility(View.GONE);
        button_Expand.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.bttn_recyclerview_row_expand) {
            if (linearLayout_childItems.getVisibility() == View.VISIBLE) {
                linearLayout_childItems.setVisibility(View.GONE);
            } else {
                linearLayout_childItems.setVisibility(View.VISIBLE);
            }
        } else {
            TextView textViewClicked = (TextView) view;
            Toast.makeText(mContext, "" + textViewClicked.getText().toString(), Toast.LENGTH_SHORT).show();
        }

        //TEsting Zone
        Observer<CityDataModel> obs = new Observer<CityDataModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(CityDataModel cityDataModel) {

                Log.e(TAG,"ITS WORKING...");
                Log.e(TAG,cityDataModel.getCityName()+" "+cityDataModel.getRequestTime());

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };



    }
    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
}
