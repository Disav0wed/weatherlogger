package com.ibrahimrecepserpici.weatherlogger.rxbus;

import com.ibrahimrecepserpici.weatherlogger.pojo.CitiesInRange;

import io.reactivex.subjects.PublishSubject;

// This Class is created for establish communictaion between OpenWeatherApi and DetailsFragmentPresenter.
public class RxCitiesInRangeBus {

    public PublishSubject<CitiesInRange> publishSubject = null;

    private static RxCitiesInRangeBus RxBusSingleTonObject = null;


    private RxCitiesInRangeBus()
    {
        publishSubject = PublishSubject.create();
    }

    //Singleton
    public static RxCitiesInRangeBus getInstance()
    {

        if(RxBusSingleTonObject ==null)
        {
            RxBusSingleTonObject = new RxCitiesInRangeBus();
        }

        return RxBusSingleTonObject;
    }
}
