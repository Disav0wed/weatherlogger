package com.ibrahimrecepserpici.weatherlogger.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "weather_records")
public class CityDataModel {

    public CityDataModel(int id, String latitude, String longitude,String temperature, String cityName, String windSpeed, String windDirection, String humidity, String pressure, String weatherStatus, String requestTime) {
        this.id = id;
        this.temperature = temperature;
        this.cityName = cityName;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
        this.humidity = humidity;
        this.pressure = pressure;
        this.weatherStatus = weatherStatus;
        this.requestTime = requestTime;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Ignore
    public CityDataModel(String latitude, String longitude, String temperature, String cityName, String windSpeed, String windDirection, String humidity, String pressure, String weatherStatus, String requestTime) {

        this.temperature = temperature;
        this.cityName = cityName;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
        this.humidity = humidity;
        this.pressure = pressure;
        this.weatherStatus = weatherStatus;
        this.requestTime = requestTime;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;



    @ColumnInfo(name = "city_name")
    private String cityName;

    @ColumnInfo(name = "latitude ")
    private String latitude ;



    @ColumnInfo(name = "longitude")
    private String longitude;

    @ColumnInfo(name = "temperature")
    private String temperature;

    @ColumnInfo(name = "wind_speed")
    private String windSpeed;

    @ColumnInfo(name = "wind_direction")
    private String windDirection;

    @ColumnInfo(name = "humidity")
    private String humidity;

    @ColumnInfo(name = "pressure")
    private String pressure;

    @ColumnInfo(name = "weather_status")
    private String weatherStatus;

    @ColumnInfo(name = "time_stamp")
    private String requestTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getWeatherStatus() {
        return weatherStatus;
    }

    public void setWeatherStatus(String weatherStatus) {
        this.weatherStatus = weatherStatus;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }



}
