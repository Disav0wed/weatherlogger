package com.ibrahimrecepserpici.weatherlogger.database;
import android.content.Context;
import android.util.Log;

import androidx.room.Room;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;

public class DatabaseManager {

    private static final String TAG = DatabaseManager.class.getSimpleName();
    private Context mContext;
    private AppDatabase mAppDatabase;

    public DatabaseManager(Context context)
    {
        this.mContext = context;
        mAppDatabase = AppDatabase.getInstance(mContext);

        Log.e(TAG,"CREATED");
        AppDatabase db = Room.databaseBuilder(mContext,
                AppDatabase.class, "weatherloggerdatabase").build();
    }


    public void Insert(final CityDataModel cityDataModel)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mAppDatabase.cityDao().insert(cityDataModel);
            }
        }).start();
    }

    public void InsertAll()
    {

    }

    public void Request()
    {

    }

    public Observable<List<CityDataModel>> RequestAll()
    {
        Observable<List<CityDataModel>> resultList = new Observable<List<CityDataModel>>() {
            @Override
            protected void subscribeActual(Observer<? super List<CityDataModel>> observer) {
                List<CityDataModel> tempList;
                tempList = mAppDatabase.cityDao().getAll();
                observer.onNext(tempList);
            }
        };


        return  resultList;


    }
    public void DeleteAll()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {

                mAppDatabase.cityDao().nukeTable();

            }
        }).start();
    }
}
