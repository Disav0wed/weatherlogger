package com.ibrahimrecepserpici.weatherlogger.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;


import java.util.List;

@Dao
public interface CityDao {

    @Query("SELECT * FROM weather_records")
    List<CityDataModel> getAll();

    @Query("SELECT * FROM weather_records WHERE time_stamp IN (:timeStamp)")
    List<CityDataModel> loadAllByIds(String timeStamp);

    @Insert
    void insert(CityDataModel city);


    @Insert
    void insertAll(CityDataModel... city);

    @Delete
    void delete(CityDataModel city);

    @Query("DELETE FROM weather_records")
    public void nukeTable();

}
