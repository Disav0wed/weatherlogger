package com.ibrahimrecepserpici.weatherlogger.model;

import android.util.Log;

import com.ibrahimrecepserpici.weatherlogger.database.AppDatabase;
import com.ibrahimrecepserpici.weatherlogger.pojo.CitiesInRange;
import com.ibrahimrecepserpici.weatherlogger.rxbus.RxCitiesInRangeBus;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class DetailsFragmentDataHandler implements Observer<CitiesInRange> {
    private static final String TAG = DetailsFragmentDataHandler.class.getSimpleName();
    private DataHandlerCallback mDataHandlerCallback;

    public DetailsFragmentDataHandler(DataHandlerCallback dataHandlerCallback)
    {
        this.mDataHandlerCallback = dataHandlerCallback;

        //Subscribe this class to bus so it can get data which emitted by OpenWeatherApi
        RxCitiesInRangeBus.getInstance().publishSubject.subscribe(this);
    }



    @Override
    public void onSubscribe(Disposable d) {
        Log.e(TAG,"onSubscribe: Called.");

    }

    @Override
    public void onNext(CitiesInRange citiesInRange) {
        Log.e(TAG,"onNext: Called.");
        mDataHandlerCallback.onWeatherInformationReceived(citiesInRange);



    }

    @Override
    public void onError(Throwable e) {
        Log.e(TAG,"onError: Called. Error is : "+e.toString());

    }

    @Override
    public void onComplete() {
        Log.e(TAG,"onComplete: Called.");

    }

    public interface DataHandlerCallback
    {
        void onWeatherInformationReceived(CitiesInRange citiesInRange);
    }
}
