package com.ibrahimrecepserpici.weatherlogger.model;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.ibrahimrecepserpici.weatherlogger.network.RetrofitClientInstance;
import com.ibrahimrecepserpici.weatherlogger.network.RetrofitInterface;
import com.ibrahimrecepserpici.weatherlogger.pojo.CitiesInRange;
import com.ibrahimrecepserpici.weatherlogger.pojo.City;
import com.ibrahimrecepserpici.weatherlogger.rxbus.RxCitiesInRangeBus;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OpenWeatherApiManager implements Observer<CitiesInRange> {

    private static final String TAG = OpenWeatherApiManager.class.getSimpleName();
    private static final String OPENWEATHER_API_KEY = "ef4b95610e6c6105424d5d1a83482180";
    private RetrofitInterface service;
    private OpenWeatherCallback mOpenWeatherCallback;



    public OpenWeatherApiManager(OpenWeatherCallback openWeatherCallback)
    {
        this.mOpenWeatherCallback = openWeatherCallback;
        /*Create handle for the RetrofitInstance interface*/
        service = RetrofitClientInstance.getRetrofitInstance().create(RetrofitInterface .class);
        Log.e(TAG,"Created");
    }

    public void fetchWeatherAsync(LatLng coord)
    {
        Log.e(TAG,"fetchWeatherAsync: Called");


        service.getWeatherInCircle(String.valueOf(coord.latitude),String.valueOf(coord.longitude),"5",OPENWEATHER_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this);
    }


    @Override
    public void onSubscribe(Disposable d) {
        Log.e(TAG,"onSubscribe: Called");

    }

    @Override
    public void onNext(CitiesInRange citiesInRange) {
        Log.e(TAG,"onNext: Called");

        for(City c : citiesInRange.getCityList())
        {
            Log.e(TAG,c.getName());
        }
        mOpenWeatherCallback.onNearestCitiesInfoRecieved(citiesInRange);

        //Publish Data on bus so DetailsFragmentDataHandler Receive it
        RxCitiesInRangeBus.getInstance().publishSubject.onNext(citiesInRange);

    }

    @Override
    public void onError(Throwable e) {
        Log.e(TAG,"onError: Called. Error : "+e.toString());
    }

    @Override
    public void onComplete() {

        Log.e(TAG,"onComplete: Called");
    }


    public interface OpenWeatherCallback
    {
        void onCityInfoReceived(City city);
        void onNearestCitiesInfoRecieved(CitiesInRange citiesInRange);
    }

}
