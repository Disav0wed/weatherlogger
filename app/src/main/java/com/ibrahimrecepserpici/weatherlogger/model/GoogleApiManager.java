package com.ibrahimrecepserpici.weatherlogger.model;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.PermissionChecker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

public class GoogleApiManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ResultCallback<LocationSettingsResult> {

    private static final String TAG = GoogleApiManager.class.getSimpleName();

    private static final int LOCATION_REQUEST_INTERVAL = 20000;
    private static final int LOCATION_REQUEST_FASTEST_INTERVAL = 10000;
    private static final int LOCATION_REQUEST_PRIORITY = LocationRequest.PRIORITY_HIGH_ACCURACY;

    private GoogleApiClient mGoogleApiClient;
    private LocationSettingsRequest.Builder mLocationSettingsRequestBuilder;
    private LocationRequest mLocationRequest;
    private Location mLastKnownLocation;
    private PendingResult<LocationSettingsResult> pendingResult;
    private Context mContext;
    private LocationCallBack locationCallBack;



    public GoogleApiManager(Activity activity, LocationCallBack locationCallBack){

        mContext = activity.getApplicationContext();
        this.locationCallBack = locationCallBack;
        mGoogleApiClient = new GoogleApiClient.Builder(activity.getApplicationContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        this.mLocationRequest = new LocationRequest();
        this.mLocationRequest.setInterval(LOCATION_REQUEST_INTERVAL);
        this.mLocationRequest.setFastestInterval(LOCATION_REQUEST_FASTEST_INTERVAL);
        this.mLocationRequest.setPriority(LOCATION_REQUEST_PRIORITY);
        this.mLocationSettingsRequestBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(this.mLocationRequest);
        connect();

    }

    public void connect() {
        Log.d(TAG, "connect: Called");
        mGoogleApiClient.connect();
    }

    public void removeLocationUpdates()
    {
        Log.d(TAG, "removeLocationUpdates: Called");

        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);

    }



    public void disconnect() {

        removeLocationUpdates();
        mGoogleApiClient.disconnect();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG,"onConnected: Called");

        if (PermissionChecker.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                PermissionChecker.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "onConnected: permissions not granted");
            Toast.makeText(mContext, "Please Enable Permissions" , Toast.LENGTH_LONG)                    .show();
            return;
        }



        pendingResult = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                mLocationSettingsRequestBuilder.build());

        mLastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

        if (mLastKnownLocation != null) {
            Log.i(TAG, "onConnected: lat " + mLastKnownLocation.getLatitude());
            Log.i(TAG, "onConnected: long " + mLastKnownLocation.getLongitude());
        }
        locationCallBack.onMapReady();
    }

    @Override
    public void onConnectionSuspended(int i) {
            Log.e(TAG,"onConnectionSuspended: Called");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG,"onConnectionFailed: Called");

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(TAG,"onLocationChanged: Called");

        Log.e("LOC: ","Lat : "+location.getLatitude());
        locationCallBack.onLocationChanged(location);

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        Log.d(TAG, "onResult: Called");
        final Status status = locationSettingsResult.getStatus();

        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "onResult: status - SUCCESS");
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "onResult: status - RESOLUTION_REQUIRED");
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "onResult: status - SETTINGS_CHANGE_UNAVAILABLE");
                break;
        }
    }

    public interface LocationCallBack
    {
        void onLocationChanged(Location location);
        void onMapReady();
    }
}
