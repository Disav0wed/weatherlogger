# WEATHER LOGGER

Android application that shows nearest weather info based on current location.


## Libraries & Frameworks used

RxJava (For Async data handling)
Retrofit (As Rest Api)
RoomDatabase (for saving data locally)
GoogleMaps Api (to get user location)
RecyclerView (to list all nearby city information in details fragment)


## Architecture

Model View Presenter Design Pattern Used in this Application.

## Attention!

Do not forget to enable permissions. (Application is popping up a Toast message when you do not have permission)


## How it works.

Application is fetching user's location periodically by using GoogleMaps Api.

After the location is fetched, application is calling OpenWeather Api and requesting weather info for nearest 5 city.

After weather info received weather information of related cities is displayed in both map and details tab

When save button clicked application is saving the current state of weather information on the screen to the local database.

When load button clicked application is loading the saved weather information from the local database.

## Example Images From Application

![Alt text](https://img.techpowerup.org/191215/screenshot-1576434405.png)


![Alt text](https://img.techpowerup.org/191215/screenshot-1576434434.png)


![Alt text](https://img.techpowerup.org/191215/screenshot-1576434603.png)


![Alt text](https://img.techpowerup.org/191215/screenshot-1576434456.png)


![Alt text](https://img.techpowerup.org/191215/screenshot-1576434463.png)

